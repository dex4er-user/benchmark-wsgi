if __name__ == '__main__':
    import Pyjo.Server.Daemon
    server = Pyjo.Server.Daemon.new(listen=['http://localhost:8000'])
else:
    import Pyjo.Server.WSGI
    server = Pyjo.Server.WSGI.new()

server.unsubscribe('request')

@server.on
def request(server, tx):
    content = b"Hello World!\n"
    tx.res.body = content
    tx.res.code = 200
    tx.res.headers.content_type = 'text/html; charset=utf-8'
    tx.res.headers.content_length = str(len(content))
    tx.resume()

if __name__ == '__main__':
    import Pyjo.IOLoop

    @Pyjo.IOLoop.timer(15)
    def timeout(loop):
        print("Timeout!")
        server.stop()
        Pyjo.IOLoop.stop()

    server.run()


else:
    app = server.to_wsgi_app()
