from werkzeug.serving import run_simple

from hello_wsgi import app

run_simple('localhost', 8000, app, use_reloader=True)
