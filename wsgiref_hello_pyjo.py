from wsgiref.simple_server import make_server

from hello_pyjo import app

httpd = make_server('localhost', 8000, app)
print("Serving on port 8000...")
httpd.serve_forever()
