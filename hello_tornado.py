import tornado.httpserver
import tornado.ioloop
import tornado.web
import tornado.wsgi


class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.write("Hello World!")


def main():
    application = tornado.web.Application([
        (r"/", MainHandler),
    ])
    http_server = tornado.httpserver.HTTPServer(application)
    http_server.listen(8000)
    tornado.ioloop.IOLoop.instance().start()


app = tornado.wsgi.WSGIApplication([
        (r"/", MainHandler),
      ])


if __name__ == "__main__":
    print("Serving on port 8000...")
    main()
