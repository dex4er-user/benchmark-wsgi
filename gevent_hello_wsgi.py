from gevent.wsgi import WSGIServer

from hello_wsgi import app

http_server = WSGIServer(('localhost', 8000), app)
http_server.serve_forever()
