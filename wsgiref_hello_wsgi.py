from wsgiref.simple_server import make_server

from hello_wsgi import app

httpd = make_server('localhost', 8000, app)
print("Serving on port 8000...")
httpd.serve_forever()
