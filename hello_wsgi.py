from wsgiref.util import setup_testing_defaults
from wsgiref.simple_server import make_server

# A relatively simple WSGI application. It's going to print out the
# environment dictionary after being updated by setup_testing_defaults
def app(environ, start_response):
    setup_testing_defaults(environ)

    content = "Hello World!\n"

    status = '200 OK'
    headers = [('Content-Type', 'text/html; charset=utf-8'),
               ('Content-Length', str(len(content)))]

    start_response(status, headers)

    ret = [content]
    return ret
