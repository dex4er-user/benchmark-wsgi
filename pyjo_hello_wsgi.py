import Pyjo.Server.Daemon

from hello_wsgi import app

server = Pyjo.Server.Daemon.new(listen=['http://localhost:8000'])

server.unsubscribe('request')

@server.on
def request(server, tx):
    environ = {}
    

    app(environ, start_response)
    content = b"Hello World!\n"
    tx.res.body = content
    tx.res.code = 200
    tx.res.headers.content_type = 'text/html; charset=utf-8'
    tx.res.headers.content_length = str(len(content))
    tx.resume()

server.run()
